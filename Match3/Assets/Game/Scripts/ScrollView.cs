﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ScrollView : EnhancedMonoBehaviour {
    public GameObject scrollableObject;
    public bool movementXAxis = true;
    public bool movementYAxis = true;

    private float deltaTouchX = 0;
    private float deltaTouchY = 0;
    private float currentSpeedX = 0;
    private float currentSpeedY = 0;
    private const float FINAL_ACCELERATION_XY = 0;
    private bool executeSpeedX = false;
    private bool executeSpeedY = false;
    public float maxSpeed = 1000;
    private float minSpeed;
    private const float decreaseSpeed = 10;
    protected override void Awake() {
        base.Awake();
        minSpeed = -maxSpeed;
        EnableTouchEvents();
        OnTouchStarted = delegate () {
            Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if(movementXAxis) {
                deltaTouchX = scrollableObject.transform.position.x - mouseWorldPos.x;
                executeSpeedX = false;
            }
            if(movementYAxis) {
                deltaTouchY = scrollableObject.transform.position.y - mouseWorldPos.y;
                executeSpeedY = false;
            }
        };
        OnTouchHolding = delegate() {
            Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            float posX = transform.position.x;
            float posY = transform.position.y;
            if (movementXAxis) {
                posX = mouseWorldPos.x + deltaTouchX;
                currentSpeedX = (posX - scrollableObject.transform.position.x) / Time.deltaTime;
            }
            if (movementYAxis) {
                posY = mouseWorldPos.y + deltaTouchY;
                currentSpeedY = (posY - scrollableObject.transform.position.y) / Time.deltaTime;
            }

            scrollableObject.transform.position = new Vector3(posX, posY, transform.position.z);
        };
        OnTouchEnded = delegate () {
            if (movementXAxis) {
                if (currentSpeedX > maxSpeed) {
                    currentSpeedX = maxSpeed;
                } else if (currentSpeedX < minSpeed) {
                    currentSpeedX = minSpeed;
                }
                executeSpeedX = true;
            }

            if (movementYAxis) {
                if (currentSpeedY > maxSpeed) {
                    currentSpeedY = maxSpeed;
                } else if (currentSpeedY < minSpeed) {
                    currentSpeedY = minSpeed;
                }
                executeSpeedY = true;
            }
        };
    }

    protected override void Update() {
        base.Update();
        if (executeSpeedX || executeSpeedY) {
            if (executeSpeedX) {
                if (currentSpeedX >= 0) {
                    currentSpeedX -= decreaseSpeed;
                    if (currentSpeedX <= 0) {
                        currentSpeedX = 0;
                        executeSpeedX = false;
                    }
                } else {
                    currentSpeedX += decreaseSpeed;
                    if (currentSpeedX >= 0) {
                        currentSpeedX = 0;
                        executeSpeedX = false;
                    }
                }
            }
            if (executeSpeedY) {
                if (currentSpeedY >= 0) {
                    currentSpeedY -= decreaseSpeed;
                    if (currentSpeedY <= 0) {
                        currentSpeedY = 0;
                        executeSpeedY = false;
                    }
                } else {
                    currentSpeedY += decreaseSpeed;
                    if (currentSpeedY >= 0) {
                        currentSpeedY = 0;
                        executeSpeedY = false;
                    }
                }
            }
            scrollableObject.transform.Translate(currentSpeedX * Time.deltaTime, currentSpeedY * Time.deltaTime, 0);
        }
    }
}
