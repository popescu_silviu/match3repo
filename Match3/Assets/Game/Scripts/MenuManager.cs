﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

public class MenuManager : MonoBehaviour
{
	[SerializeField]
	private GameObject levelItemPrefab;

	[SerializeField]
	private Transform levelItemsParent;

	[SerializeField]
	private TextAsset[] levelTextAssets;

	public static MenuManager instance = null;
	void Awake () {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);

		foreach (TextAsset levelTextAsset in levelTextAssets)
		{
			var levelItemObject = Instantiate(levelItemPrefab, levelItemsParent);
			var levelItem = levelItemObject.GetComponent<LevelItem>();
			levelItem.Bind(int.Parse(Regex.Replace(levelTextAsset.name, "[^0-9.]", "")));
		}
	}
}
