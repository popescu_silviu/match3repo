﻿
public static class Constants {
	public const float CAMERA_WIDTH = 480;
	public const float CAMERA_HEIGHT = 800;
	public const float CAMERA_HALFT_WIDTH = 240;
	public const float CAMERA_HALFT_HEIGHT = 400;
	public const float FALLING_SPEED = 0.0015f;

    public const int NO_SWAP = 0;
    public const int SWAP = 1;
    public const int USER_HORIZONTAL_SWAP = 2;
    public const int USER_VERTICAL_SWAP = 3;
    public const int SWAP_DIRECTION_NOT_IMPORTANT = 4;

    public const int CHECK_FOR_SIMPLE_MATCHES = 1;
    public const int CHECK_FOR_ALL_MATCHES = 2;

    public const int FALL_ITEMS_IN_MATRIX = 0;
    public const int DONT_FALL_ITEMS_IN_MATRIX = 1;

    public const int LEFT = 0;
    public const int RIGHT = 1;
}
