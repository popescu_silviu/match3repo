﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnhancedMonoBehaviour : MonoBehaviour {
    public enum EaseFunction {
        NO_EASE, EASE_IN, EASE_OUT, OUT_BACK
    }

    protected SpriteRenderer spriteRenderer;
    protected virtual void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        TouchEndedOutsideCollider = false;
        TouchEndedInsideCollider = false;
        TouchHoldingOutsideCollider = false;
        TouchHoldingInsideCollider = false;
    }

    bool executeMove = false;
    float moveX, moveY;
    EaseFunction easeFunctionMove;
    float durationMove, startX, startY, elapsedTimeMove, changeInValueMoveX, changeInValueMoveY;
    public void Move(float newX, float newY, float duration, EaseFunction easeFunction) {
        moveX = newX;
        moveY = newY;
        executeMove = true;
        durationMove = duration;
        startX = transform.position.x;
        startY = transform.position.y;
        elapsedTimeMove = 0;
        changeInValueMoveX = newX - startX;
        changeInValueMoveY = newY - startY;
        easeFunctionMove = easeFunction;
    }
    public void MoveX(float newX, float duration, EaseFunction easeFunction) {
        moveX = newX;
        moveY = transform.position.y;
        executeMove = true;
        durationMove = duration;
        startX = transform.position.x;
        startY = transform.position.y;
        elapsedTimeMove = 0;
        changeInValueMoveX = newX - startX;
        changeInValueMoveY = 0;
        easeFunctionMove = easeFunction;
    }
    public void MoveY(float newY, float duration, EaseFunction easeFunction) {
        moveX = transform.position.x;
        moveY = newY;
        executeMove = true;
        durationMove = duration;
        startX = transform.position.x;
        startY = transform.position.y;
        elapsedTimeMove = 0;
        changeInValueMoveX = 0;
        changeInValueMoveY = newY - startY;
        easeFunctionMove = easeFunction;
    }
    public void MoveStop() {
        executeMove = false;
    }

    bool executeAlpha = false;
    float durationAlpha, startAlpha, finalAlpha, elapsedTimeAlpha, changeInValueAlpha;
    EaseFunction easeFunctionAlpha;
    public void AlphaTo(float alpha, float time, EaseFunction easeFunction) {
        finalAlpha = alpha;
        startAlpha = spriteRenderer.color.a;
        changeInValueAlpha = finalAlpha - startAlpha;
        executeAlpha = true;
        durationAlpha = time;
        easeFunctionAlpha = easeFunction;
    }
    public void AlphaStop() {
        executeAlpha = false;
    }

    bool executeRotation = false;
	float newAngle, rotateSpeed;
	public void RotateByAmount (float amount, float time) {
		newAngle = transform.eulerAngles.z + amount;
		rotateSpeed = amount / time;
		executeRotation = true;
	}
	public void RotateTo (float rotationAngle, float time) {
		newAngle = rotationAngle;
		float angleDifference = rotationAngle - transform.eulerAngles.z;
		rotateSpeed = Mathf.Abs (angleDifference / time);
		executeRotation = true;
	}
    public void RotateStop() {
        executeRotation = false;
    }

    bool executeScale = false;
    float finalScaleX, finalScaleY, durationScale, startScaleX, startScaleY, elapsedTimeScale, changeInValueScaleX, changeInValueScaleY;
    EaseFunction easeFunctionScale;
    public void ScaleTo(float newScaleX, float newScaleY, float duration, EaseFunction easeFunction) {
        finalScaleX = newScaleX;
        finalScaleY = newScaleY;
        executeScale = true;
        durationScale = duration;
        startScaleX = transform.localScale.x;
        startScaleY = transform.localScale.y;
        elapsedTimeScale = 0;
        changeInValueScaleX = finalScaleX - startScaleX;
        changeInValueScaleY = finalScaleY - startScaleY;
        easeFunctionScale = easeFunction;
    }
    public void ScaleStop() {
        executeScale = false;
    }

    public class Delay {
        public const float NEXT_FRAME = -1;
		public float DelayDuration { get; set; }
		public float ElapsedTime { get; set; }
		public DelayCallBackFunction DelayCallBack { get; set; }
		public Delay (float delayDuration, DelayCallBackFunction delayCallBack) {
			DelayDuration = delayDuration;
			DelayCallBack = delayCallBack;
			ElapsedTime = 0;
		}
	}

	public delegate void DelayCallBackFunction ();
	List<Delay> delaysList = new List<Delay> ();
	public void AddDelay (float time, DelayCallBackFunction delayCallBack) {
		Delay newDelay = new Delay (time, delayCallBack);
		delaysList.Add (newDelay);
	}

	bool touchEnabled = false, touchHolding = false, executeTouchEnded = false;
    public bool TouchEndedOutsideCollider { get; set; }
    public bool TouchEndedInsideCollider { get; set; }
    public bool TouchHoldingOutsideCollider { get; set; }
    public bool TouchHoldingInsideCollider { get; set; }
    BoxCollider2D boxCollider2D;
	public delegate void OnTouchStartedFunction ();
	public OnTouchStartedFunction OnTouchStarted;
	public delegate void OnTouchHoldingFunction ();
	public OnTouchHoldingFunction OnTouchHolding;
	public delegate void OnTouchEndedFunction ();
	public OnTouchEndedFunction OnTouchEnded;
    private bool hasScrollViewParent;
    private Vector3 initMouseWorldPos;
    public void EnableTouchEvents () {
		touchEnabled = true;
		boxCollider2D = gameObject.AddComponent<BoxCollider2D> ();

        Transform parentObject = transform.parent;
        while (parentObject != null) {
            if (parentObject.gameObject.GetComponent<ScrollView>() != null) {
                hasScrollViewParent = true;
                break;
            }
            parentObject = parentObject.parent;
        }
	}
    Vector3 deltaTouch = new Vector3();
    bool followTouch = false;
    public void FollowTouch () {
        followTouch = true;
    }
    public void UnfollowTouch () {
        followTouch = false;
    }

    protected virtual void Update() {
        if (executeMove) {
            elapsedTimeMove += Time.deltaTime;
            float posX, posY;
            if (easeFunctionMove == EaseFunction.EASE_OUT) {
                posX = EaseEquations.easeOut(changeInValueMoveX, elapsedTimeMove, durationMove, startX);
                posY = EaseEquations.easeOut(changeInValueMoveY, elapsedTimeMove, durationMove, startY);
            } else if (easeFunctionMove == EaseFunction.EASE_IN) {
                posX = EaseEquations.easeIn(changeInValueMoveX, elapsedTimeMove, durationMove, startX);
                posY = EaseEquations.easeIn(changeInValueMoveY, elapsedTimeMove, durationMove, startY);
            } else if (easeFunctionMove == EaseFunction.OUT_BACK) {
                posX = EaseEquations.outBack(changeInValueMoveX, elapsedTimeMove, durationMove, startX);
                posY = EaseEquations.outBack(changeInValueMoveY, elapsedTimeMove, durationMove, startY);
            } else {
                posX = EaseEquations.noEase(changeInValueMoveX, elapsedTimeMove, durationMove, startX);
                posY = EaseEquations.noEase(changeInValueMoveY, elapsedTimeMove, durationMove, startY);
            }
            transform.position = new Vector2(posX, posY);
            if (elapsedTimeMove >= durationMove) {
                executeMove = false;
                transform.position = new Vector2(moveX, moveY);
            }
        }
        if (executeAlpha) {
            elapsedTimeAlpha += Time.deltaTime;
            float spriteRendererAlpha;
            if (easeFunctionAlpha == EaseFunction.EASE_OUT) {
                spriteRendererAlpha = EaseEquations.easeOut(changeInValueAlpha, elapsedTimeAlpha, durationAlpha, startAlpha);
            } else if (easeFunctionAlpha == EaseFunction.EASE_IN) {
                spriteRendererAlpha = EaseEquations.easeIn(changeInValueAlpha, elapsedTimeAlpha, durationAlpha, startAlpha);
            } else if (easeFunctionAlpha == EaseFunction.OUT_BACK) {
                spriteRendererAlpha = EaseEquations.outBack(changeInValueAlpha, elapsedTimeAlpha, durationAlpha, startAlpha);
            } else {
                spriteRendererAlpha = EaseEquations.noEase(changeInValueAlpha, elapsedTimeAlpha, durationAlpha, startAlpha);
            }
            spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, spriteRendererAlpha);
            if (elapsedTimeAlpha >= durationAlpha) {
                executeAlpha = false;
                spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, finalAlpha);
            }
        }
        if (executeRotation) {
			transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.Euler (transform.rotation.x, transform.rotation.y, newAngle), rotateSpeed * Time.deltaTime);
			if (Mathf.Approximately (transform.eulerAngles.z, newAngle)) {
				executeRotation = false;
				transform.rotation = Quaternion.Euler (transform.rotation.x, transform.rotation.y, newAngle);
			}
		}
        if (executeScale) {
            elapsedTimeScale += Time.deltaTime;
            float scaleX, scaleY;
            if (easeFunctionScale == EaseFunction.EASE_OUT) {
                scaleX = EaseEquations.easeOut(changeInValueScaleX, elapsedTimeScale, durationScale, startScaleX);
                scaleY = EaseEquations.easeOut(changeInValueScaleY, elapsedTimeScale, durationScale, startScaleY);
            } else if (easeFunctionScale == EaseFunction.EASE_IN) {
                scaleX = EaseEquations.easeIn(changeInValueScaleX, elapsedTimeScale, durationScale, startScaleX);
                scaleY = EaseEquations.easeIn(changeInValueScaleY, elapsedTimeScale, durationScale, startScaleY);
            } else if (easeFunctionScale == EaseFunction.OUT_BACK) {
                scaleX = EaseEquations.outBack(changeInValueScaleX, elapsedTimeScale, durationScale, startScaleX);
                scaleY = EaseEquations.outBack(changeInValueScaleY, elapsedTimeScale, durationScale, startScaleY);
            } else {
                scaleX = EaseEquations.noEase(changeInValueScaleX, elapsedTimeScale, durationScale, startScaleX);
                scaleY = EaseEquations.noEase(changeInValueScaleY, elapsedTimeScale, durationScale, startScaleY);
            }
            transform.localScale = new Vector2(scaleX, scaleY);
            if (elapsedTimeScale >= durationScale) {
                executeScale = false;
                transform.localScale = new Vector2(finalScaleX, finalScaleY);
            }
        }
        for (int i = delaysList.Count - 1; i >= 0; i--){
			delaysList [i].ElapsedTime += Time.deltaTime;
			if ((delaysList [i].ElapsedTime >= delaysList [i].DelayDuration) || (delaysList[i].DelayDuration == Delay.NEXT_FRAME)) {
				delaysList [i].DelayCallBack ();
				delaysList.RemoveAt (i);
			}
		}
		if (touchEnabled) {
			if (Input.GetMouseButtonDown (0)) {
                initMouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				if (boxCollider2D.OverlapPoint (new Vector2 (initMouseWorldPos.x, initMouseWorldPos.y))) {
					if (OnTouchStarted != null) {
						OnTouchStarted ();
					}
                    if (followTouch) {
                        deltaTouch = new Vector3(transform.position.x - initMouseWorldPos.x, transform.position.y - initMouseWorldPos.y, 0);
                    }
					touchHolding = true;
					executeTouchEnded = true;
				}
			}
			if (Input.GetMouseButton (0)) {
				if (touchHolding) {
                    Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    if (boxCollider2D.OverlapPoint (new Vector2 (mouseWorldPos.x, mouseWorldPos.y))) {
						TouchHoldingInsideCollider = true;
						TouchHoldingOutsideCollider = false;
					} else {
						TouchHoldingInsideCollider = false;
						TouchHoldingOutsideCollider = true;
					}
					if (OnTouchHolding != null) {
						OnTouchHolding ();
					}
                    if (followTouch) {
                        transform.position = new Vector3(mouseWorldPos.x + deltaTouch.x, mouseWorldPos.y + deltaTouch.y, transform.position.z);
                    }
                    if (hasScrollViewParent && executeTouchEnded) {
                        if (GetDistance(initMouseWorldPos, mouseWorldPos) > 10) {
                            executeTouchEnded = false;
                        }
                    }
                }
            }
			if (Input.GetMouseButtonUp (0)) {
				if (executeTouchEnded) {
					Vector3 worldTouchPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);
					if (boxCollider2D.OverlapPoint (new Vector2 (worldTouchPoint.x, worldTouchPoint.y))) {
						TouchEndedInsideCollider = true;
						TouchEndedOutsideCollider = false;
					} else {
						TouchEndedInsideCollider = false;
						TouchEndedOutsideCollider = true;
					}
					if (OnTouchEnded != null) {
						OnTouchEnded ();
					}
					touchHolding = false;
					executeTouchEnded = false;
				}
			}
		}
	}

    public static float GetDistance(Vector2 vector1, Vector2 vector2) {
        return GetDistance(vector1.x, vector1.y, vector2.x, vector2.y);
    }

    public static float GetDistance(GameObject object1, GameObject object2) {
        return GetDistance(object1.transform.position.x, object1.transform.position.y, object2.transform.position.x, object2.transform.position.y);
    }

    public static float GetDistance(float x1, float y1, float x2, float y2) {
        return Mathf.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    }
}