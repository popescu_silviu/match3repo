﻿using UnityEngine;

public class CameraController : MonoBehaviour {
    public bool panX = true;
    public bool panY = true;
    public float maxPanSpeed = 1000;
    public bool zoom = true;

    public float perspectiveZoomSpeed = 0.5f;
    public float orthoZoomSpeed = 0.5f;

    private Vector3 panOrigin;
    private float currentSpeedX;
    private float currentSpeedY;
    private bool executeSpeedX = false;
    private bool executeSpeedY = false;
    private const float decreaseSpeed = 10;

    private void LateUpdate() {
        Pan();
        Zoom();
    }

    private void Pan() {
        if (panX || panY) {
            if (Input.GetMouseButtonDown(0)) {
                panOrigin = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
            if (Input.GetMouseButton(0)) {
                Vector3 amount = Camera.main.ScreenToWorldPoint(Input.mousePosition) - panOrigin;
                if (panX) {
                    currentSpeedX = amount.x / Time.deltaTime;
                } else {
                    amount.x = 0;
                }
                if (panY) {
                    currentSpeedY = amount.y / Time.deltaTime;
                } else {
                    amount.y = 0;
                }
                Camera.main.transform.Translate(-amount.x, -amount.y, 0);
            }
            if (Input.GetMouseButtonUp(0)) {
                if (panX) {
                    if (currentSpeedX > maxPanSpeed) {
                        currentSpeedX = maxPanSpeed;
                    } else if (currentSpeedX < -maxPanSpeed) {
                        currentSpeedX =- maxPanSpeed;
                    }
                    executeSpeedX = true;
                }
                if (panY) {
                    if (currentSpeedY > maxPanSpeed) {
                        currentSpeedY = maxPanSpeed;
                    } else if (currentSpeedY < -maxPanSpeed) {
                        currentSpeedY = -maxPanSpeed;
                    }
                    executeSpeedY = true;
                }
            }
			if (executeSpeedX || executeSpeedY) {
				if (executeSpeedX) {
					if (currentSpeedX >= 0) {
						currentSpeedX -= decreaseSpeed;
						if (currentSpeedX < 0) {
							currentSpeedX = 0;
							executeSpeedX = false;
						}
					} else {
						currentSpeedX += decreaseSpeed;
						if (currentSpeedX > 0) {
							currentSpeedX = 0;
							executeSpeedX = false;
						}
					}
				}
				if (executeSpeedY) {
					if (currentSpeedY >= 0) {
						currentSpeedY -= decreaseSpeed;
						if (currentSpeedY < 0) {
							currentSpeedY = 0;
							executeSpeedY = false;
						}
					} else {
						currentSpeedY += decreaseSpeed;
						if (currentSpeedY > 0) {
							currentSpeedY = 0;
							executeSpeedY = false;
						}
					}
				}
				transform.Translate(-currentSpeedX * Time.deltaTime, -currentSpeedY * Time.deltaTime, 0);
			}
		}
    }

    private void Zoom() {
        if (zoom && Input.touchCount == 2) {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            if (Camera.main.orthographic) {
                Camera.main.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
                Camera.main.orthographicSize = Mathf.Max(Camera.main.orthographicSize, 0.1f);
            } else {
                Camera.main.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;
                Camera.main.fieldOfView = Mathf.Clamp(Camera.main.fieldOfView, 0.1f, 179.9f);
            }
        }
    }
}
