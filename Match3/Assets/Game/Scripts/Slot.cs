﻿using UnityEngine;
using System.Collections;

public class Slot : MonoBehaviour {

    bool enabledSlot;
    public Item CurrentItem { get; set; }
    public Item FutureItem { get; set; }
    public bool HasFutureItem { get; set; }

    public SpriteRenderer backgroundSpriteRenderer;
    void Awake() {
        HasFutureItem = false;
    }

    public bool EnabledSlot {
        get {
            return enabledSlot;
        }
        set {
            enabledSlot = value;
            if (enabledSlot) {
                backgroundSpriteRenderer.enabled = true;
            } else {
                backgroundSpriteRenderer.enabled = false;
            }
        }
    }
}
