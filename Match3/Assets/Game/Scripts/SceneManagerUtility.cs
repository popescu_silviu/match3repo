﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerUtility : MonoBehaviour {
	public static SceneManagerUtility instance = null;
    public int CurrentLevel { get; set; }
	void Awake () {
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
        DontDestroyOnLoad(this);
	}

    public void UnloadMenuSceneLoadGameScene(int currentLevel) {
        SceneManager.LoadScene("GameScene");
        CurrentLevel = currentLevel;
    }

    public void unloadGameSceneLoadMenuScene() {
        SceneManager.LoadScene("MainMenuScene");
    }
}
