﻿using UnityEngine;

public class Item : EnhancedMonoBehaviour {

    public Sprite nothing, type1, type2, type3, type4, type5, type6;
    public Sprite horizontalType1, horizontalType2, horizontalType3, horizontalType4, horizontalType5, horizontalType6;
    public Sprite verticalType1, verticalType2, verticalType3, verticalType4, verticalType5, verticalType6;
    public Sprite surroundingType1, surroundingType2, surroundingType3, surroundingType4, surroundingType5, surroundingType6;
    public Sprite colorBomb;
    public Sprite blocker;

    public enum TypePrimary {
        UNKNOWN = 0, TYPE_1 = 1, TYPE_2 = 2, TYPE_3 = 3, TYPE_4 = 4, TYPE_5 = 5, TYPE_6 = 6,
        BLOCKER = 7,
        COLOR_BOMB = 8,
        NONE = 9
    }
    public enum TypeSecondary {
        NORMAL = 0, HORIZONTAL_CLEANER = 1, VERTICAL_CLEANER = 2, SURROUNDING_CLEANER = 3,
        BLOCKER_1 = 5, BLOCKER_2 = 6, BLOCKER_3 = 7
    }
    public TypePrimary TypePrimaryItem { get; set; }
	public TypeSecondary TypeSecondaryItem { get; set; }

    public int i { get; set; }
    public int j { get; set; }

    public int futureI { get; set; }
    public int futureJ { get; set; }

    public bool willFallDiagonally = false;

    protected override void Awake () {
        base.Awake();

		spriteRenderer.sortingOrder = 20;

        ResetFutureIAndJ();
    }

    public void ResetFutureIAndJ() {
        futureI = -1;
        futureJ = -1;
        willFallDiagonally = false;
    }

    static System.Random random = new System.Random();
    public void SetTypePrimary (TypePrimary typePrimary) {
        if (typePrimary == TypePrimary.UNKNOWN) {
            int randomType = random.Next(1, 501);
            if (randomType >= 1 && randomType <= 100) {
                typePrimary = TypePrimary.TYPE_1;
            } else if (randomType >= 1 && randomType <= 100) {
                typePrimary = TypePrimary.TYPE_2;
            } else if (randomType >= 101 && randomType <= 200) {
                typePrimary = TypePrimary.TYPE_3;
            } else if (randomType >= 201 && randomType <= 300) {
                typePrimary = TypePrimary.TYPE_4;
            } else if (randomType >= 301 && randomType <= 400) {
                typePrimary = TypePrimary.TYPE_5;
            } else if (randomType >= 401 && randomType <= 500) {
                typePrimary = TypePrimary.TYPE_6;
            }
        }

        TypePrimaryItem = typePrimary;
		if (TypePrimaryItem == TypePrimary.TYPE_1) {
			spriteRenderer.sprite = type1;
		} else if (TypePrimaryItem == TypePrimary.TYPE_2) {
			spriteRenderer.sprite = type2;
		} else if (TypePrimaryItem == TypePrimary.TYPE_3) {
			spriteRenderer.sprite = type3;
		} else if (TypePrimaryItem == TypePrimary.TYPE_4) {
			spriteRenderer.sprite = type4;
		} else if (TypePrimaryItem == TypePrimary.TYPE_5) {
			spriteRenderer.sprite = type5;
		} else if (TypePrimaryItem == TypePrimary.TYPE_6) {
			spriteRenderer.sprite = type6;
        } else if (TypePrimaryItem == TypePrimary.COLOR_BOMB) {
            spriteRenderer.sprite = colorBomb;
        } else if (TypePrimaryItem == TypePrimary.BLOCKER) {
            spriteRenderer.sprite = blocker;
        }
    }

	public void SetTypeSecondary (TypeSecondary typeSecondary) {
		TypeSecondaryItem = typeSecondary;
        if (TypePrimaryItem == TypePrimary.TYPE_1) {
            if (TypeSecondaryItem == TypeSecondary.HORIZONTAL_CLEANER) {
                spriteRenderer.sprite = horizontalType1;
            } else if (TypeSecondaryItem == TypeSecondary.VERTICAL_CLEANER) {
                spriteRenderer.sprite = verticalType1;
            } else if (TypeSecondaryItem == TypeSecondary.SURROUNDING_CLEANER) {
                spriteRenderer.sprite = surroundingType1;
            }
        } else if (TypePrimaryItem == TypePrimary.TYPE_2) {
            if (TypeSecondaryItem == TypeSecondary.HORIZONTAL_CLEANER) {
                spriteRenderer.sprite = horizontalType2;
            } else if (TypeSecondaryItem == TypeSecondary.VERTICAL_CLEANER) {
                spriteRenderer.sprite = verticalType2;
            } else if (TypeSecondaryItem == TypeSecondary.SURROUNDING_CLEANER) {
                spriteRenderer.sprite = surroundingType2;
            }
        } else if (TypePrimaryItem == TypePrimary.TYPE_3) {
            if (TypeSecondaryItem == TypeSecondary.HORIZONTAL_CLEANER) {
                spriteRenderer.sprite = horizontalType3;
            } else if (TypeSecondaryItem == TypeSecondary.VERTICAL_CLEANER) {
                spriteRenderer.sprite = verticalType3;
            } else if (TypeSecondaryItem == TypeSecondary.SURROUNDING_CLEANER) {
                spriteRenderer.sprite = surroundingType3;
            }
        } else if (TypePrimaryItem == TypePrimary.TYPE_4) {
            if (TypeSecondaryItem == TypeSecondary.HORIZONTAL_CLEANER) {
                spriteRenderer.sprite = horizontalType4;
            } else if (TypeSecondaryItem == TypeSecondary.VERTICAL_CLEANER) {
                spriteRenderer.sprite = verticalType4;
            } else if (TypeSecondaryItem == TypeSecondary.SURROUNDING_CLEANER) {
                spriteRenderer.sprite = surroundingType4;
            }
        } else if (TypePrimaryItem == TypePrimary.TYPE_5) {
            if (TypeSecondaryItem == TypeSecondary.HORIZONTAL_CLEANER) {
                spriteRenderer.sprite = horizontalType5;
            } else if (TypeSecondaryItem == TypeSecondary.VERTICAL_CLEANER) {
                spriteRenderer.sprite = verticalType5;
            } else if (TypeSecondaryItem == TypeSecondary.SURROUNDING_CLEANER) {
                spriteRenderer.sprite = surroundingType5;
            }
        } else if (TypePrimaryItem == TypePrimary.TYPE_6) {
            if (TypeSecondaryItem == TypeSecondary.HORIZONTAL_CLEANER) {
                spriteRenderer.sprite = horizontalType6;
            } else if (TypeSecondaryItem == TypeSecondary.VERTICAL_CLEANER) {
                spriteRenderer.sprite = verticalType6;
            } else if (TypeSecondaryItem == TypeSecondary.SURROUNDING_CLEANER) {
                spriteRenderer.sprite = surroundingType6;
            }
        }
    }
}
