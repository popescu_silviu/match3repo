﻿using UnityEngine;
using System.Collections;

public class EaseEquations : MonoBehaviour
{

	public static float easeIn(float changeInValue, float elapsedTime, float duration, float startValue)
	{
		elapsedTime /= duration;
		return changeInValue * elapsedTime * elapsedTime + startValue;
	}

	public static float easeOut(float changeInValue, float elapsedTime, float duration, float startValue)
	{
		elapsedTime /= duration;
		return -changeInValue * elapsedTime * (elapsedTime - 2) + startValue;
	}

	public static float outBack(float changeInValue, float elapsedTime, float duration, float startValue)
	{
		elapsedTime /= duration;
		float ts = elapsedTime * elapsedTime;
		float tc = ts * elapsedTime;
		return startValue + changeInValue * (-6.345f * tc * ts + 12.6425f * ts * ts - 2.2f * tc - 9.195f * ts + 6.0975f * elapsedTime);
	}

	public static float noEase(float changeInValue, float elapsedTime, float duration, float startValue)
	{
		return changeInValue * elapsedTime / duration + startValue;
	}



}
