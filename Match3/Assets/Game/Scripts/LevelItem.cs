using TMPro;
using UnityEngine;

public class LevelItem : MonoBehaviour
{
    [SerializeField]
    private TMP_Text titleText;

    private int levelNumber;

    private void Awake()
    {

    }

    public void Bind(int levelNumber)
    {
        this.levelNumber = levelNumber;
        titleText.text = levelNumber.ToString();
    }

    public void OnClicked()
    {
        SceneManagerUtility.instance.UnloadMenuSceneLoadGameScene(levelNumber);
    } 
}
