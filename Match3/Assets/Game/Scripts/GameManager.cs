﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml;
using System;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class GameManager : EnhancedMonoBehaviour
{
    [SerializeField]
    private GameObject slotPrefab;

    [SerializeField]
    private GameObject itemPrefab;

    [SerializeField]
    private int scorePerTile = 100;

    [SerializeField]
    private float speedConstant = 0.002f;

    [SerializeField]
    private TMP_Text scoreText;

    [SerializeField]
    private Button backButton;

    [SerializeField]
    private Camera gameCamera;

    [SerializeField]
    private int cameraReferenceRows = 7;

    [SerializeField]
    private int cameraReferenceColumns = 5;

    int score;
    Slot[,] matrix = new Slot[10, 10];
    float spawnYPos;
    float timeBetween2Rows, timeDiagonalFall;
    bool receiveInput = true;
    int timesShuffled = 0;
    int rows = 0, columns = 0;
    bool randomizeItemTypes = false;
    static System.Random random = new System.Random();

    private int currentLevel;
    protected override void Awake()
    {
        base.Awake();
        if (SceneManagerUtility.instance == null)
        {
            currentLevel = 1;
            backButton.gameObject.SetActive(false);
        }
        else
        {
            currentLevel = SceneManagerUtility.instance.CurrentLevel;
        }
        Setup();
        ManageCamera();
    }

    private void ManageCamera()
    {
        float rowsRatio = (float)cameraReferenceRows / rows;
        float columnsRatio = (float)cameraReferenceColumns / columns;
        float referenceRatio = Mathf.Min(rowsRatio, columnsRatio);
        gameCamera.orthographicSize *= 1 / referenceRatio;
    }

    public void Setup()
    {
        TextAsset xmlAsset = (TextAsset)Resources.Load(Path.Combine("Levels", "level" + currentLevel), typeof(TextAsset));
        XmlTextReader xmlReader = new XmlTextReader(new StringReader(xmlAsset.text));
        xmlReader.Read();
        xmlReader.Read();
        xmlReader.Read();
        if (xmlReader.Name.Equals("rows"))
        {
            rows = xmlReader.ReadElementContentAsInt();
        }
        if (xmlReader.Name.Equals("columns"))
        {
            columns = xmlReader.ReadElementContentAsInt();
        }
        if (xmlReader.Name.Equals("randomizeItemTypes"))
        {
            randomizeItemTypes = bool.Parse(xmlReader.ReadElementContentAsString());
        }

        List<int> typePrimaryValues = new List<int>() { (int)Item.TypePrimary.TYPE_1, (int)Item.TypePrimary.TYPE_2, (int)Item.TypePrimary.TYPE_3, (int)Item.TypePrimary.TYPE_4, (int)Item.TypePrimary.TYPE_5, (int)Item.TypePrimary.TYPE_6 };
        int numberOfTypes = typePrimaryValues.Count;
        List<int> typePrimaryValuesKeys = new List<int>(typePrimaryValues);
        Dictionary<int, int> typePrimaryValuesCorrespondents = new Dictionary<int, int>();
        if (randomizeItemTypes)
        {
            for (int i = 0; i < numberOfTypes; i++)
            {
                int pos = random.Next(typePrimaryValues.Count);
                typePrimaryValuesCorrespondents.Add(typePrimaryValuesKeys[i], typePrimaryValues[pos]);
                typePrimaryValues.RemoveAt(pos);
            }
        }
        else
        {
            for (int i = 0; i < numberOfTypes; i++)
            {
                typePrimaryValuesCorrespondents.Add(typePrimaryValuesKeys[i], typePrimaryValues[i]);
            }
        }

        float space = 5;
        float paddingBetweenX = itemPrefab.GetComponent<SpriteRenderer>().bounds.size.x + space;
        float paddingBetweenY = itemPrefab.GetComponent<SpriteRenderer>().bounds.size.y + space;

        float coefficientX;
        if (columns % 2 == 0)
        {
            coefficientX = 0.5f;
        }
        else
        {
            coefficientX = 0;
        }
        float currentX = (coefficientX - (columns / 2)) * itemPrefab.GetComponent<SpriteRenderer>().bounds.size.x - ((columns - 1) / 2) * space;
        float currentY = -paddingBetweenY / 2f;
        
        timeBetween2Rows = paddingBetweenY * speedConstant;
        timeDiagonalFall = (float)Math.Sqrt((paddingBetweenX * paddingBetweenX + paddingBetweenY * paddingBetweenY)) * speedConstant;

        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (xmlReader.Name.Equals("slot"))
                {
                    matrix[i, j] = ((GameObject)Instantiate(slotPrefab, new Vector3(currentX, currentY, 1), Quaternion.identity, transform)).GetComponent<Slot>();
                    matrix[i, j].EnabledSlot = bool.Parse(xmlReader.GetAttribute("enabled"));
                    if (matrix[i, j].EnabledSlot)
                    {
                        xmlReader.Read();
                        if (xmlReader.Name.Equals("item"))
                        {
                            Item.TypePrimary typePrimary = Item.TypePrimary.UNKNOWN;
                            Item.TypeSecondary typeSecondary = Item.TypeSecondary.NORMAL;
                            xmlReader.Read();
                            if (xmlReader.Name.Equals("typePrimary"))
                            {
                                string primaryTypeString = xmlReader.ReadElementContentAsString();
                                int primaryTypeKey = (int)Enum.Parse(typeof(Item.TypePrimary), primaryTypeString);
                                if (typePrimaryValuesKeys.Contains(primaryTypeKey))
                                {
                                    typePrimary = (Item.TypePrimary)typePrimaryValuesCorrespondents[primaryTypeKey];
                                }
                                else
                                {
                                    typePrimary = (Item.TypePrimary)Enum.Parse(typeof(Item.TypePrimary), primaryTypeString);
                                }
                            }
                            if (xmlReader.Name.Equals("typeSecondary"))
                            {
                                try
                                {
                                    typeSecondary = (Item.TypeSecondary)Enum.Parse(typeof(Item.TypeSecondary), xmlReader.ReadElementContentAsString());
                                }
                                catch
                                {
                                    typeSecondary = Item.TypeSecondary.NORMAL;
                                }
                            }
                            Item newItem = ((GameObject)Instantiate(itemPrefab, new Vector3(matrix[i, j].transform.position.x, matrix[i, j].transform.position.y, 0), Quaternion.identity, transform)).GetComponent<Item>();
                            newItem.SetTypePrimary(typePrimary);
                            newItem.SetTypeSecondary(typeSecondary);
                            matrix[i, j].CurrentItem = newItem;
                            matrix[i, j].CurrentItem.i = i;
                            matrix[i, j].CurrentItem.j = j;
                            if (IsItemMovable(i, j))
                            {
                                AddTouchEvents(matrix[i, j].CurrentItem);
                            }

                            xmlReader.Read();
                            xmlReader.Read();
                        }
                    }
                    else
                    {
                        xmlReader.Read();
                    }
                    currentX += paddingBetweenX;
                }
            }
            currentX = (coefficientX - (columns / 2)) * itemPrefab.GetComponent<SpriteRenderer>().bounds.size.x - ((columns - 1) / 2) * space;
            currentY -= paddingBetweenY;
        }

        var position = transform.position;
        position.y = -currentY / 2f;
        transform.position = position;

        spawnYPos = position.y + paddingBetweenY;
    }

    int user_swap_direction;
    void AddTouchEvents(Item item)
    {
        item.EnableTouchEvents();
        item.OnTouchEnded = delegate ()
        {
            if (receiveInput)
            {
                int i = item.i;
                int j = item.j;

                var checkMatchResult = CheckMatchForItem(i, j, Constants.SWAP, Constants.CHECK_FOR_ALL_MATCHES);
                if (checkMatchResult)
                {
                    float delay = FallItems();
                    AddDelay(delay, delegate ()
                    {
                        CheckMatchForAllItems(Constants.SWAP);
                    });
                }
            }
        };
    }

    void CheckMatchForAllItems(int checkMode)
    {
        var thereAreItemsToFall = CheckIfThereAreItemsToFall(Constants.DONT_FALL_ITEMS_IN_MATRIX);
        var thareAreNeighboursToProvideItems = CheckIfNeighboursCanProvideItems(Constants.DONT_FALL_ITEMS_IN_MATRIX);

        if (thereAreItemsToFall || thareAreNeighboursToProvideItems)
        {
            receiveInput = false;
            float delay = FallItems();
            AddDelay(delay, delegate ()
            {
                CheckMatchForAllItems(checkMode);
            });
        }
        else
        {
            if (!CheckPossibleFutureMatches())
            {
                if (timesShuffled == 0)
                {
                    receiveInput = false;
                    for (int i = 0; i < rows; i++)
                    {
                        for (int j = 0; j < columns; j++)
                        {
                            if (matrix[i, j] != null && matrix[i, j].CurrentItem != null && IsItemMovable(i, j))
                            {
                                if (matrix[i, j].CurrentItem.transform.position.x != 0 || matrix[i, j].CurrentItem.transform.position.y != 0)
                                {
                                    matrix[i, j].CurrentItem.Move(0, 0, 0.5f, EaseFunction.NO_EASE);
                                }
                            }
                        }
                    }
                    AddDelay(1, delegate ()
                    {
                        ShuffleItems();
                        CheckMatchForAllItems(Constants.NO_SWAP);
                    });
                }
                else
                {
                    ShuffleItems();
                    CheckMatchForAllItems(Constants.NO_SWAP);
                }
            }
            else
            {
                if (timesShuffled > 0)
                {
                    for (int i = 0; i < rows; i++)
                    {
                        for (int j = 0; j < columns; j++)
                        {
                            if (matrix[i, j] != null && matrix[i, j].CurrentItem != null && IsItemMovable(i, j))
                            {
                                if (matrix[i, j].CurrentItem.transform.position.x != matrix[i, j].transform.position.x || matrix[i, j].CurrentItem.transform.position.y != matrix[i, j].transform.position.y)
                                {
                                    matrix[i, j].CurrentItem.Move(matrix[i, j].transform.position.x, matrix[i, j].transform.position.y, 0.5f, EaseFunction.NO_EASE);
                                }
                            }
                        }
                    }
                    AddDelay(0.5f, delegate ()
                    {
                        timesShuffled = 0;
                        receiveInput = true;
                    });
                }
                else
                {
                    receiveInput = true;
                }
            }
        }
    }

    bool CheckPossibleFutureMatches()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                var checkMatchFirstItem = CheckMatchForItem(i, j, Constants.NO_SWAP, Constants.CHECK_FOR_ALL_MATCHES);
                if (checkMatchFirstItem)
                {
                    return true;
                }
            }
        }
        return false;
    }

    void ShuffleItems()
    {
        timesShuffled++;
        if (timesShuffled > 1)
        {
            bool found = false;
            int i;
            int j;
            while (!found)
            {
                i = random.Next(0, rows);
                j = random.Next(0, columns);
                if (matrix[i, j] != null && matrix[i, j].CurrentItem != null && IsItemMovable(i, j))
                {
                    found = true;
                    RemoveItemSolo(i, j);
                    Item newItem = ((GameObject)Instantiate(itemPrefab, new Vector3(0, 0, 0), Quaternion.identity, transform)).GetComponent<Item>();
                    newItem.SetTypePrimary(Item.TypePrimary.UNKNOWN);
                    newItem.SetTypeSecondary(Item.TypeSecondary.NORMAL);
                    matrix[i, j].CurrentItem = newItem;
                    matrix[i, j].CurrentItem.i = i;
                    matrix[i, j].CurrentItem.j = j;
                    AddTouchEvents(newItem);
                }
            }
        }
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (matrix[i, j] != null && matrix[i, j].CurrentItem != null && IsItemMovable(i, j))
                {
                    int otherI = random.Next(0, rows);
                    int otherJ = random.Next(0, columns);
                    if (matrix[otherI, otherJ] != null && matrix[otherI, otherJ].CurrentItem != null && IsItemMovable(otherI, otherJ))
                    {
                        Item aux = matrix[i, j].CurrentItem;
                        matrix[i, j].CurrentItem = matrix[otherI, otherJ].CurrentItem;
                        matrix[otherI, otherJ].CurrentItem = aux;

                        matrix[i, j].CurrentItem.i = i;
                        matrix[i, j].CurrentItem.j = j;
                        matrix[otherI, otherJ].CurrentItem.i = otherI;
                        matrix[otherI, otherJ].CurrentItem.j = otherJ;
                    }
                }
            }
        }
    }

    bool IsItemMovable(int i, int j)
    {
        Item.TypePrimary typePrimary = matrix[i, j].CurrentItem.TypePrimaryItem;
        if (typePrimary == Item.TypePrimary.BLOCKER)
        {
            return false;
        }
        return true;
    }

    bool CheckMatchForItem(int i, int j, int checkMode, int checkType)
    {
        var item = matrix[i, j].CurrentItem;
        if (item != null && IsItemMovable(i, j))
        {
            var primaryType = matrix[i, j].CurrentItem.TypePrimaryItem;
            var secondaryType = matrix[i, j].CurrentItem.TypeSecondaryItem;

            var matchingNeighbours = GetMatchingNeighbours(i, j);

            int newI, newJ;
            int minI = i, maxI = i, minJ = j, maxJ = j;
            newI = i - 1;
            while (newI >= 0 && matrix[newI, j] != null && matrix[newI, j].CurrentItem != null && matrix[newI, j].CurrentItem.TypePrimaryItem == primaryType)
            {
                minI = newI;
                newI--;
            }
            newI = i + 1;
            while (newI < rows && matrix[newI, j] != null && matrix[newI, j].CurrentItem != null && matrix[newI, j].CurrentItem.TypePrimaryItem == primaryType)
            {
                maxI = newI;
                newI++;
            }
            newJ = j - 1;
            while (newJ >= 0 && matrix[i, newJ] != null && matrix[i, newJ].CurrentItem != null && matrix[i, newJ].CurrentItem.TypePrimaryItem == primaryType)
            {
                minJ = newJ;
                newJ--;
            }
            newJ = j + 1;
            while (newJ < columns && matrix[i, newJ] != null && matrix[i, newJ].CurrentItem != null && matrix[i, newJ].CurrentItem.TypePrimaryItem == primaryType)
            {
                maxJ = newJ;
                newJ++;
            }

            float differenceI = maxI - minI;
            float differenceJ = maxJ - minJ;

            Item newItem = null;
            if (checkType == Constants.CHECK_FOR_ALL_MATCHES)
            {
                if (matchingNeighbours.Count >= 5)
                {
                    if (checkMode != Constants.NO_SWAP)
                    {
                        newItem = Instantiate(itemPrefab, new Vector3(matrix[i, j].transform.position.x, matrix[i, j].transform.position.y, 0), Quaternion.identity, transform).GetComponent<Item>();
                        newItem.SetTypePrimary(Item.TypePrimary.COLOR_BOMB);
                        newItem.SetTypeSecondary(Item.TypeSecondary.NORMAL);
                    }
                }
                else if (matchingNeighbours.Count >= 4)
                {
                    if (checkMode != Constants.NO_SWAP)
                    {
                        newItem = Instantiate(itemPrefab, new Vector3(matrix[i, j].transform.position.x, matrix[i, j].transform.position.y, 0), Quaternion.identity, transform).GetComponent<Item>();
                        newItem.SetTypePrimary(primaryType);
                        newItem.SetTypeSecondary(Item.TypeSecondary.SURROUNDING_CLEANER);
                    }
                }
                else if (differenceI == 3 || differenceJ == 3)
                {
                    if (checkMode != Constants.NO_SWAP)
                    {
                        if (differenceI == 3)
                        {
                            if (user_swap_direction == Constants.SWAP_DIRECTION_NOT_IMPORTANT)
                            {
                                i = random.Next(minI, maxI + 1);
                            }
                        }
                        else if (differenceJ == 3)
                        {
                            if (user_swap_direction == Constants.SWAP_DIRECTION_NOT_IMPORTANT)
                            {
                                j = random.Next(minJ, maxJ + 1);
                            }
                        }
                        newItem = Instantiate(itemPrefab, new Vector3(matrix[i, j].transform.position.x, matrix[i, j].transform.position.y, 0), Quaternion.identity, transform).GetComponent<Item>();
                        newItem.SetTypePrimary(primaryType);
                        if (user_swap_direction == Constants.USER_HORIZONTAL_SWAP)
                        {
                            newItem.SetTypeSecondary(Item.TypeSecondary.HORIZONTAL_CLEANER);
                        }
                        else if (user_swap_direction == Constants.USER_VERTICAL_SWAP)
                        {
                            newItem.SetTypeSecondary(Item.TypeSecondary.VERTICAL_CLEANER);
                        }
                        else
                        {
                            int randomTypeSecondary = random.Next(0, 2);
                            if (randomTypeSecondary == 0)
                            {
                                newItem.SetTypeSecondary(Item.TypeSecondary.HORIZONTAL_CLEANER);
                            }
                            else
                            {
                                newItem.SetTypeSecondary(Item.TypeSecondary.VERTICAL_CLEANER);
                            }
                        }
                    }
                }
            }

            var isPopwerUp = primaryType == Item.TypePrimary.COLOR_BOMB
                    || secondaryType == Item.TypeSecondary.HORIZONTAL_CLEANER
                    || secondaryType == Item.TypeSecondary.VERTICAL_CLEANER
                    || secondaryType == Item.TypeSecondary.SURROUNDING_CLEANER;

            if (checkMode != Constants.NO_SWAP
                && checkType == Constants.CHECK_FOR_ALL_MATCHES || checkType == Constants.CHECK_FOR_SIMPLE_MATCHES)
            {
                if (matchingNeighbours.Count > 0 || isPopwerUp)
                {
                    RemoveItem(i, j);
                    IncreaseScore();
                    foreach (var neighbourItem in matchingNeighbours)
                    {
                        RemoveItem(neighbourItem.i, neighbourItem.j);
                        IncreaseScore();
                    }
                }
                else
                {
                    var scaleTime = .15f;
                    item.ScaleTo(1.2f, 1.2f, scaleTime, EaseFunction.EASE_OUT);
                    AddDelay(scaleTime, () =>
                    {
                        item.ScaleTo(1f, 1f, scaleTime, EaseFunction.EASE_OUT);
                    });
                }
            }

            if (checkMode != Constants.NO_SWAP && newItem != null)
            {
                matrix[i, j].CurrentItem = newItem;
                matrix[i, j].CurrentItem.i = i;
                matrix[i, j].CurrentItem.j = j;
                AddTouchEvents(newItem);
            }

            return matchingNeighbours.Count > 0 || isPopwerUp;
        }
        return false;
    }

    List<Item> GetMatchingNeighbours(int i, int j, List<Item> searchedItems = null)
    {
        var matchingNeighbours = new List<Item>();
        if (i < 0 || j < 0 || i >= rows || j >= columns)
        {
            return matchingNeighbours;
        }
        if (searchedItems == null)
        {
            searchedItems = new List<Item>();
        }
        searchedItems.Add(matrix[i, j].CurrentItem);
        var typePrimaryItem = matrix[i, j].CurrentItem.TypePrimaryItem;
        if (!searchedItems.Any(x => x.i == i && x.j == j + 1)
            && i >= 0 && j + 1 >= 0
            && i < rows && j + 1 < columns
            && matrix[i, j + 1] != null
            && matrix[i, j + 1].CurrentItem != null
            && matrix[i, j + 1].CurrentItem.TypePrimaryItem == typePrimaryItem)
        {
            matchingNeighbours.Add(matrix[i, j + 1].CurrentItem);
            matchingNeighbours.AddRange(GetMatchingNeighbours(i, j + 1, searchedItems));
        }
        if (!searchedItems.Any(x => x.i == i && x.j == j - 1)
            && i >= 0 && j - 1 >= 0
            && i < rows && j - 1 < columns
            && matrix[i, j - 1] != null
            && matrix[i, j - 1].CurrentItem != null
            && matrix[i, j - 1].CurrentItem.TypePrimaryItem == typePrimaryItem)
        {
            matchingNeighbours.Add(matrix[i, j - 1].CurrentItem);
            matchingNeighbours.AddRange(GetMatchingNeighbours(i, j - 1, searchedItems));
        }
        if (!searchedItems.Any(x => x.i == i + 1 && x.j == j)
            && i + 1 >= 0 && j >= 0
            && i + 1 < rows && j < columns
            && matrix[i + 1, j] != null
            && matrix[i + 1, j].CurrentItem != null
            && matrix[i + 1, j].CurrentItem.TypePrimaryItem == typePrimaryItem)
        {
            matchingNeighbours.Add(matrix[i + 1, j].CurrentItem);
            matchingNeighbours.AddRange(GetMatchingNeighbours(i + 1, j, searchedItems));
        }
        if (!searchedItems.Any(x => x.i == i - 1 && x.j == j)
            && i - 1 >= 0 && j >= 0
            && i - 1 < rows && j < columns
            && matrix[i - 1, j] != null
            && matrix[i - 1, j].CurrentItem != null
            && matrix[i - 1, j].CurrentItem.TypePrimaryItem == typePrimaryItem)
        {
            matchingNeighbours.Add(matrix[i - 1, j].CurrentItem);
            matchingNeighbours.AddRange(GetMatchingNeighbours(i - 1, j, searchedItems));
        }
        return matchingNeighbours;
    }

    void RemoveItem(int i, int j)
    {
        if (matrix[i, j] != null && matrix[i, j].CurrentItem != null)
        {
            if (matrix[i, j].CurrentItem.TypePrimaryItem == Item.TypePrimary.COLOR_BOMB)
            {
                RemoveItemSolo(i, j);

                var availableItemsToRemove = new List<Item>();
                for (int k = 0; k < rows; k++)
                {
                    for (int l = 0; l < columns; l++)
                    {
                        var item = matrix[k, l].CurrentItem;
                        if (item != null)
                        {
                            var type = item.TypePrimaryItem;
                            if (type == Item.TypePrimary.TYPE_1
                                || type == Item.TypePrimary.TYPE_2
                                || type == Item.TypePrimary.TYPE_3
                                || type == Item.TypePrimary.TYPE_4
                                || type == Item.TypePrimary.TYPE_5
                                || type == Item.TypePrimary.TYPE_6)
                            {
                                availableItemsToRemove.Add(item);
                            }
                        }
                    }
                }
                Item.TypePrimary typeToRemove = Item.TypePrimary.TYPE_1;
                if (availableItemsToRemove.Count > 0)
                {
                    typeToRemove = availableItemsToRemove[UnityEngine.Random.Range(0, availableItemsToRemove.Count)].TypePrimaryItem;
                }

                for (int k = 0; k < rows; k++)
                {
                    for (int l = 0; l < columns; l++)
                    {
                        var item = matrix[k, l].CurrentItem;
                        if (item != null
                            && item.TypePrimaryItem == typeToRemove)
                        {
                            RemoveItem(k, l);
                        }
                    }
                }
            }
            else if (matrix[i, j].CurrentItem.TypeSecondaryItem == Item.TypeSecondary.NORMAL)
            {
                if (IsItemMovable(i, j))
                {
                    if (i - 1 >= 0 && matrix[i - 1, j] != null && matrix[i - 1, j].CurrentItem != null && !IsItemMovable(i - 1, j))
                    {
                        RemoveItem(i - 1, j);
                    }
                    if (i + 1 < rows && matrix[i + 1, j] != null && matrix[i + 1, j].CurrentItem != null && !IsItemMovable(i + 1, j))
                    {
                        RemoveItem(i + 1, j);
                    }
                    if (j - 1 >= 0 && matrix[i, j - 1] != null && matrix[i, j - 1].CurrentItem != null && !IsItemMovable(i, j - 1))
                    {
                        RemoveItem(i, j - 1);
                    }
                    if (j + 1 < columns && matrix[i, j + 1] != null && matrix[i, j + 1].CurrentItem != null && !IsItemMovable(i, j + 1))
                    {
                        RemoveItem(i, j + 1);
                    }
                }
                RemoveItemSolo(i, j);
            }
            else if (matrix[i, j].CurrentItem.TypeSecondaryItem == Item.TypeSecondary.HORIZONTAL_CLEANER)
            {
                RemoveItemSolo(i, j);
                for (int k = 0; k < columns; k++)
                {
                    if (k != j && matrix[i, k] != null && matrix[i, k].CurrentItem != null)
                    {
                        RemoveItem(i, k);
                    }
                }
            }
            else if (matrix[i, j].CurrentItem.TypeSecondaryItem == Item.TypeSecondary.VERTICAL_CLEANER)
            {
                RemoveItemSolo(i, j);
                for (int k = 0; k < rows; k++)
                {
                    if (k != i && matrix[k, j] != null && matrix[k, j].CurrentItem != null)
                    {
                        RemoveItem(k, j);
                    }
                }
            }
            else if (matrix[i, j].CurrentItem.TypeSecondaryItem == Item.TypeSecondary.SURROUNDING_CLEANER)
            {
                RemoveItemSolo(i, j);
                for (int k = i - 1; k <= i + 1; k++)
                {
                    for (int l = j - 1; l <= j + 1; l++)
                    {
                        if (k >= 0 && k < rows && l >= 0 && l < columns)
                        {
                            if (!(k == i && l == j) && matrix[k, l] != null && matrix[k, l].CurrentItem != null)
                            {
                                RemoveItem(k, l);
                            }
                        }
                    }
                }
            }
        }
    }

    void RemoveItemSolo(int i, int j)
    {
        var item = matrix[i, j].CurrentItem;
        matrix[i, j].CurrentItem = null;

        var scaleDuration = .5f;
        item.ScaleTo(0, 0, .5f, EaseFunction.EASE_OUT);
        AddDelay(scaleDuration, () =>
        {
            Destroy(item.gameObject);
        });
    }

    private void IncreaseScore()
    {
        score += scorePerTile;
        scoreText.text = $"Score: {score}";
    }

    float FallItems()
    {
        ResetAllFutureItemsAndSlotsParameters();

        CheckIfThereAreItemsToFall(Constants.FALL_ITEMS_IN_MATRIX);

        GenerateItemsVertically();

        CheckIfNeighboursCanProvideItems(Constants.FALL_ITEMS_IN_MATRIX);

        float maxDelay = VerticalFalling(0);

        maxDelay += FallingFromNeighbours(maxDelay);

        return maxDelay;
    }

    bool CheckIfThereAreItemsToFall(int fallItemsInMatrixMode)
    {
        bool thereAreItemsToFall = false;
        for (int j = 0; j < columns; j++)
        {
            for (int i = rows - 1; i >= 0; i--)
            {
                if (matrix[i, j] != null && matrix[i, j].EnabledSlot && matrix[i, j].CurrentItem == null)
                {
                    for (int k = i - 1; k >= 0; k--)
                    {
                        if (matrix[k, j].CurrentItem != null)
                        {
                            if (IsItemMovable(k, j))
                            {
                                if (fallItemsInMatrixMode == Constants.FALL_ITEMS_IN_MATRIX)
                                {
                                    matrix[i, j].CurrentItem = matrix[k, j].CurrentItem;
                                    matrix[i, j].CurrentItem.i = i;
                                    matrix[i, j].CurrentItem.j = j;

                                    matrix[k, j].CurrentItem = null;
                                }

                                thereAreItemsToFall = true;
                            }
                            break;
                        }
                    }
                }
            }
        }
        return thereAreItemsToFall;
    }

    bool CheckIfNeighboursCanProvideItems(int fallItemsInMatrixMode)
    {
        bool thereAreNeighboursToProvideItems = false;
        for (int j = 0; j < columns; j++)
        {
            int direction;
            direction = Constants.RIGHT;
            for (int i = 0; i < rows; i++)
            {
                if (matrix[i, j] != null && matrix[i, j].CurrentItem != null && IsItemMovable(i, j))
                {
                    Item currentItem = matrix[i, j].CurrentItem;

                    int newILeft = -1, newIRight = -1, newJLeft = -1, newJRight = -1;

                    int newI, newJ;

                    bool condition1 = false;
                    bool condition2 = false;
                    newI = i + 1;
                    newJ = j - 1;
                    if (newI < rows && newJ >= 0 && matrix[newI, newJ] != null && matrix[newI, newJ].EnabledSlot && !matrix[newI, newJ].HasFutureItem)
                    {
                        if (matrix[newI, newJ].CurrentItem == null)
                        {
                            condition1 = true;
                        }
                        else if (matrix[newI, newJ].CurrentItem.willFallDiagonally)
                        {
                            for (int k = newI - 1; k >= 0; k--)
                            {
                                if (matrix[k, newJ] != null && matrix[k, newJ].CurrentItem != null)
                                {
                                    if (IsItemMovable(k, newJ))
                                    {
                                        condition2 = false;
                                    }
                                    else
                                    {
                                        condition2 = true;
                                    }
                                    break;
                                }
                            }
                        }
                        if (condition1 || condition2)
                        {
                            newILeft = newI;
                            newJLeft = newJ;
                            thereAreNeighboursToProvideItems = true;
                            currentItem.willFallDiagonally = true;
                        }
                    }
                    if (newILeft == -1 && newJRight == -1)
                    {
                        condition1 = false;
                        condition2 = false;
                        newI = i + 1;
                        newJ = j - 1;
                        if (newI < rows && newJ >= 0 && matrix[newI, newJ] != null && matrix[newI, newJ].EnabledSlot && !matrix[newI, newJ].HasFutureItem)
                        {
                            if (matrix[newI, newJ].CurrentItem == null)
                            {
                                condition1 = true;
                            }
                            else if (matrix[newI, newJ].CurrentItem.willFallDiagonally)
                            {
                                for (int k = newI - 1; k >= 0; k--)
                                {
                                    if (matrix[k, newJ] != null && matrix[k, newJ].CurrentItem != null)
                                    {
                                        if (IsItemMovable(k, newJ))
                                        {
                                            condition2 = false;
                                        }
                                        else
                                        {
                                            condition2 = true;
                                        }
                                        break;
                                    }
                                }
                            }
                            if (condition1 || condition2)
                            {
                                newILeft = newI;
                                newJLeft = newJ;
                                thereAreNeighboursToProvideItems = true;
                                currentItem.willFallDiagonally = true;
                            }
                        }
                    }

                    condition1 = false;
                    condition2 = false;
                    newI = i + 1;
                    newJ = j + 1;
                    if (newI < rows && newJ >= 0 && matrix[newI, newJ] != null && matrix[newI, newJ].EnabledSlot && !matrix[newI, newJ].HasFutureItem)
                    {
                        if (matrix[newI, newJ].CurrentItem == null)
                        {
                            condition1 = true;
                        }
                        else if (matrix[newI, newJ].CurrentItem.willFallDiagonally)
                        {
                            for (int k = newI - 1; k >= 0; k--)
                            {
                                if (matrix[k, newJ] != null && matrix[k, newJ].CurrentItem != null)
                                {
                                    if (IsItemMovable(k, newJ))
                                    {
                                        condition2 = false;
                                    }
                                    else
                                    {
                                        condition2 = true;
                                    }
                                    break;
                                }
                            }
                        }
                        if (condition1 || condition2)
                        {
                            newIRight = newI;
                            newJRight = newJ;
                            thereAreNeighboursToProvideItems = true;
                            currentItem.willFallDiagonally = true;
                        }
                    }
                    if (newIRight == -1 && newJRight == -1)
                    {
                        condition1 = false;
                        condition2 = false;
                        newI = i + 1;
                        newJ = j + 1;
                        if (newI < rows && newJ >= 0 && matrix[newI, newJ] != null && matrix[newI, newJ].EnabledSlot && !matrix[newI, newJ].HasFutureItem)
                        {
                            if (matrix[newI, newJ].CurrentItem == null)
                            {
                                condition1 = true;
                            }
                            else if (matrix[newI, newJ].CurrentItem.willFallDiagonally)
                            {
                                for (int k = newI - 1; k >= 0; k--)
                                {
                                    if (matrix[k, newJ] != null && matrix[k, newJ].CurrentItem != null)
                                    {
                                        if (IsItemMovable(k, newJ))
                                        {
                                            condition2 = false;
                                        }
                                        else
                                        {
                                            condition2 = true;
                                        }
                                        break;
                                    }
                                }
                            }
                            if (condition1 || condition2)
                            {
                                newIRight = newI;
                                newJRight = newJ;
                                thereAreNeighboursToProvideItems = true;
                                currentItem.willFallDiagonally = true;
                            }
                        }
                    }

                    if (fallItemsInMatrixMode == Constants.FALL_ITEMS_IN_MATRIX)
                    {
                        if (direction == Constants.LEFT && newJLeft != -1)
                        {
                            currentItem.futureI = newILeft;
                            currentItem.futureJ = newJLeft;
                            matrix[newILeft, newJLeft].HasFutureItem = true;
                            break;
                        }
                        else if (direction == Constants.RIGHT && newJRight != -1)
                        {
                            currentItem.futureI = newIRight;
                            currentItem.futureJ = newJRight;
                            matrix[newIRight, newJRight].HasFutureItem = true;
                            break;
                        }
                        else if (newJLeft != -1)
                        {
                            currentItem.futureI = newILeft;
                            currentItem.futureJ = newJLeft;
                            matrix[newILeft, newJLeft].HasFutureItem = true;
                            break;
                        }
                        else if (newJRight != -1)
                        {
                            currentItem.futureI = newIRight;
                            currentItem.futureJ = newJRight;
                            matrix[newIRight, newJRight].HasFutureItem = true;
                            break;
                        }
                    }
                }
            }
        }
        return thereAreNeighboursToProvideItems;
    }

    void GenerateItemsVertically()
    {
        for (int j = 0; j < columns; j++)
        {
            for (int k = 0; k < rows; k++)
            {
                if (matrix[k, j] != null)
                {
                    if (matrix[k, j].EnabledSlot && matrix[k, j].CurrentItem == null)
                    {
                        Item newItem = ((GameObject)Instantiate(itemPrefab, new Vector3(matrix[k, j].transform.position.x, spawnYPos, 0), Quaternion.identity)).GetComponent<Item>();
                        newItem.SetTypePrimary(Item.TypePrimary.UNKNOWN);
                        newItem.SetTypeSecondary(Item.TypeSecondary.NORMAL);
                        matrix[k, j].CurrentItem = newItem;
                        matrix[k, j].CurrentItem.i = k;
                        matrix[k, j].CurrentItem.j = j;
                        AddTouchEvents(newItem);
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }

    float VerticalFalling(float startDelay)
    {
        float totalDelay = startDelay;
        for (int j = 0; j < columns; j++)
        {
            float delay = startDelay;
            for (int i = rows - 1; i >= 0; i--)
            {
                if (matrix[i, j] != null && matrix[i, j].CurrentItem != null && IsItemMovable(i, j))
                {
                    if (matrix[i, j].CurrentItem.transform.position.y != matrix[i, j].transform.position.y)
                    {
                        Slot currentSlot = matrix[i, j];
                        float distance = currentSlot.CurrentItem.transform.position.y - currentSlot.transform.position.y;
                        float time = distance * speedConstant;
                        Item item = currentSlot.CurrentItem;
                        float pX = currentSlot.transform.position.x;
                        float pY = currentSlot.transform.position.y;

                        AddDelay(delay, delegate ()
                        {
                            if (item != null)
                            {
                                item.Move(pX, pY, time, EaseFunction.NO_EASE);
                            }
                        });

                        currentSlot.CurrentItem.i = i;
                        currentSlot.CurrentItem.j = j;

                        delay += timeBetween2Rows;
                        if (delay + time > totalDelay)
                        {
                            totalDelay = delay + time;
                        }
                    }
                }
            }
        }
        return totalDelay;
    }

    float FallingFromNeighbours(float startDelay)
    {
        float delay = 0;
        for (int j = 0; j < columns; j++)
        {
            for (int i = rows - 1; i >= 0; i--)
            {
                if (matrix[i, j] != null && matrix[i, j].CurrentItem != null && IsItemMovable(i, j))
                {
                    Slot oldSlot = matrix[i, j];
                    int futureI = oldSlot.CurrentItem.futureI;
                    int futureJ = oldSlot.CurrentItem.futureJ;
                    if (futureI != -1 && futureJ != -1)
                    {
                        Slot currentSlot = matrix[futureI, futureJ];
                        currentSlot.FutureItem = oldSlot.CurrentItem;

                        Item item = currentSlot.FutureItem;
                        float pX = currentSlot.transform.position.x;
                        float pY = currentSlot.transform.position.y;

                        item.i = futureI;
                        item.j = futureJ;

                        delay = timeDiagonalFall;

                        if (oldSlot.FutureItem == null)
                        {
                            oldSlot.CurrentItem = null;
                        }
                        AddDelay(startDelay, delegate ()
                        {
                            item.Move(pX, pY, timeDiagonalFall, EaseFunction.NO_EASE);
                        });
                        break;
                    }
                }
            }
        }

        for (int j = 0; j < columns; j++)
        {
            for (int i = rows - 1; i >= 0; i--)
            {
                if (matrix[i, j] != null && matrix[i, j].FutureItem != null)
                {
                    matrix[i, j].CurrentItem = matrix[i, j].FutureItem;
                    matrix[i, j].FutureItem = null;
                }
            }
        }

        return delay;
    }

    void ResetAllFutureItemsAndSlotsParameters()
    {
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (matrix[i, j] != null && matrix[i, j].CurrentItem != null)
                {
                    matrix[i, j].CurrentItem.ResetFutureIAndJ();
                    matrix[i, j].FutureItem = null;
                    matrix[i, j].HasFutureItem = false;
                }
            }
        }
    }

    public void BackButtonAction()
    {
        SceneManagerUtility.instance.unloadGameSceneLoadMenuScene();
    }
}