﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System;

public class Match3Editor : EditorWindow {
    const int MIN_ROWS = 5, MAX_ROWS = 10, MIN_COLUMNS = 5, MAX_COLUMNS = 10;
    static List<string> dropDownOptions;
    static Tile[,] matrixTiles = new Tile[MAX_ROWS, MAX_COLUMNS];
    XmlWriter xmlWriter;
    static XmlReader xmlReader;
    string[] selectedModeOptions = new string[] { "Add/remove slots", "Add items" };
    string[] toolbarTilesOptions = new string[] { "Add slot", "Remove slot" };
    string[] toolbarItemsOptions = new string[] { Tile.TypePrimary.UNKNOWN.ToString(), Tile.TypePrimary.TYPE_1.ToString(), Tile.TypePrimary.TYPE_2.ToString(), Tile.TypePrimary.TYPE_3.ToString(), Tile.TypePrimary.TYPE_4.ToString(), Tile.TypePrimary.TYPE_5.ToString(), Tile.TypePrimary.TYPE_6.ToString(), Tile.TypePrimary.BLOCKER.ToString(), Tile.TypePrimary.COLOR_BOMB.ToString(), "NONE" };
    string[] toolbarSelectedItemStateOptions = new string[] { Tile.TypeSecondary.NORMAL.ToString(), Tile.TypeSecondary.HORIZONTAL_CLEANER.ToString(), Tile.TypeSecondary.VERTICAL_CLEANER.ToString(), Tile.TypeSecondary.SURROUNDING_CLEANER.ToString() };
    string[] toolbarSelectedBlockerStateOptions = new string[] { Tile.TypeSecondary.BLOCKER_1.ToString(), Tile.TypeSecondary.BLOCKER_2.ToString(), Tile.TypeSecondary.BLOCKER_3.ToString() };

    Vector2 scrollPosition;
    static int level;
    static int rows, columns;
    static bool randomizeItemTypes;
    static int selectedMode;
    static int toolbarAddRemove;
    static int toolbarSelectItem;
    static int toolbarSeletectedItemState;
    static int toolbarSelectedBlockerState;
    Color disabledColor = new Color(0.5f, 0.5f, 0.5f, 0.2f);

    bool changeCurrentLevel = false;
    int newLevel;

    [MenuItem("Window/Match 3 Editor")]
    static void ShowWindow() {
        EditorWindow.GetWindow(typeof(Match3Editor));
    }

    private void OnFocus()
    {
        dropDownOptions = new List<string>();
        DirectoryInfo directory = new DirectoryInfo(Application.dataPath + "/Resources/Levels");
        FileInfo[] files = directory.GetFiles();
        foreach (FileInfo file in files)
        {
            if (file.Extension.Equals(".xml"))
            {
                string nameToShow = Path.GetFileNameWithoutExtension(file.FullName);
                dropDownOptions.Add(nameToShow);
            }
        }

        for (int i = 0; i < MAX_ROWS; i++)
        {
            for (int j = 0; j < MAX_COLUMNS; j++)
            {
                matrixTiles[i, j] = new Tile(0, 0);
            }
        }

        if (dropDownOptions.Count != 0)
        {
            LoadLevel(0);
        }
        else
        {
            CreateLevelPopup window = new CreateLevelPopup();
            window.ShowUtility();
            window.Focus();
        }
    }

    public void ResetLevelCreation() {
        for (int i = 0; i < MAX_ROWS; i++) {
            for (int j = 0; j < MAX_COLUMNS; j++) {
                matrixTiles[i, j] = new Tile(0, 0);
            }
        }

        rows = 5;
        columns = 5;
        selectedMode = 1;
        toolbarAddRemove = 0;
        toolbarSelectItem = 0;
        toolbarSeletectedItemState = 0;
        toolbarSelectedBlockerState = 0;
        randomizeItemTypes = false;
    }

    public void AddLevelToDropdown(int levelNumber) {
        ResetLevelCreation();
        string name = "level" + levelNumber;
        dropDownOptions.Add(name);
        dropDownOptions.Sort();
        for (int i = 0; i < dropDownOptions.Count; i++) {
            if (dropDownOptions[i].Equals(name)) {
                newLevel = i;
                SaveLevel(i);
            }
        }
        changeCurrentLevel = true;
    }

    void OnGUI() {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition);

        if (GUILayout.Button("Create level", GUILayout.Width(100)))
        {
            CreateLevelPopup window = ScriptableObject.CreateInstance<CreateLevelPopup>();
            window.ShowUtility();
            window.Focus();
        }

        EditorGUILayout.BeginHorizontal();
        level = EditorGUILayout.Popup("Choose a level", level, dropDownOptions.ToArray(), GUILayout.Width(300));
        if (changeCurrentLevel)
        {
            level = newLevel;
            changeCurrentLevel = false;
        }
        if (GUILayout.Button("Load Level", GUILayout.Width(100)))
        {
            LoadLevel(level);
        }
        EditorGUILayout.EndHorizontal();

        rows = EditorGUILayout.IntSlider("Number of rows", rows, MIN_ROWS, MAX_ROWS, GUILayout.Width(300));
        columns = EditorGUILayout.IntSlider("Number of columns", columns, MIN_COLUMNS, MAX_COLUMNS, GUILayout.Width(300));

        randomizeItemTypes = GUILayout.Toggle(randomizeItemTypes, "Randomize item types");

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Select edit mode");
        selectedMode = GUILayout.SelectionGrid(selectedMode, selectedModeOptions, 2, GUILayout.Width(300));
        EditorGUILayout.Space();
        EditorGUILayout.Space();

        if (selectedMode == 0)
        {
            toolbarAddRemove = GUILayout.Toolbar(toolbarAddRemove, toolbarTilesOptions);
        }

        if (selectedMode == 1)
        {
            toolbarSelectItem = GUILayout.Toolbar(toolbarSelectItem, toolbarItemsOptions);
            if (toolbarSelectItem >= 1)
            {
                if (toolbarSelectItem <= 6)
                {
                    toolbarSeletectedItemState = GUILayout.Toolbar(toolbarSeletectedItemState, toolbarSelectedItemStateOptions);
                }
                else if (toolbarSelectItem == 7)
                {
                    toolbarSelectedBlockerState = GUILayout.Toolbar(toolbarSelectedBlockerState, toolbarSelectedBlockerStateOptions);
                }
            }
        }

        for (int i = 0; i < rows; i++)
        {
            EditorGUILayout.BeginHorizontal();
            for (int j = 0; j < columns; j++)
            {
                string text = matrixTiles[i, j].ButtonText;
                if (matrixTiles[i, j].Enabled)
                {
                    GUI.color = matrixTiles[i, j].ColorTile;
                }
                else
                {
                    GUI.color = disabledColor;
                }
                if (GUILayout.Button(text, GUILayout.Width(80), GUILayout.Height(80)))
                {
                    if (selectedMode == 0)
                    {
                        if (toolbarAddRemove == 0)
                        {
                            matrixTiles[i, j].Enabled = true;
                        }
                        else if (toolbarAddRemove == 1)
                        {
                            matrixTiles[i, j].Enabled = false;
                        }
                    }
                    else if (selectedMode == 1)
                    {
                        matrixTiles[i, j].ChangePrimaryType(toolbarSelectItem);
                        matrixTiles[i, j].ChangeSecondaryType(toolbarSeletectedItemState);
                    }
                }
            }
            EditorGUILayout.EndHorizontal();
        }
        GUI.color = Color.white;
        if (GUILayout.Button("Save", GUILayout.Width(50)))
        {
            SaveLevel(level);
        }
        if (GUILayout.Button("Delete", GUILayout.Width(50)))
        {
            DeleteLevelPopup window = ScriptableObject.CreateInstance<DeleteLevelPopup>();
            window.ShowUtility();
            window.Focus();
        }
        GUILayout.EndScrollView();
    }

    public void SaveLevel(int levelNumber) {
        using (xmlWriter = XmlWriter.Create(Application.dataPath + "/Resources/Levels/" + dropDownOptions[levelNumber] + ".xml")) {
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("Level");

            xmlWriter.WriteElementString("rows", rows + "");
            xmlWriter.WriteElementString("columns", columns + "");
            xmlWriter.WriteElementString("randomizeItemTypes", randomizeItemTypes.ToString());
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    xmlWriter.WriteStartElement("slot");
                    xmlWriter.WriteAttributeString("enabled", matrixTiles[i, j].Enabled.ToString());
                    if (matrixTiles[i, j].Enabled) {
                        if (matrixTiles[i, j].TypePrimaryTile != Tile.TypePrimary.NONE) {
                            xmlWriter.WriteStartElement("item");
                            xmlWriter.WriteElementString("typePrimary", matrixTiles[i, j].TypePrimaryTile.ToString());
                            xmlWriter.WriteElementString("typeSecondary", matrixTiles[i, j].TypeSecondaryTile.ToString());
                            xmlWriter.WriteEndElement();
                        }
                    }
                    xmlWriter.WriteEndElement();
                }
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
        }
        AssetDatabase.Refresh();
    }

    public static void LoadLevel(int levelNumber) {
        using (xmlReader = XmlReader.Create(Application.dataPath + "/Resources/Levels/" + dropDownOptions[levelNumber] + ".xml")) {
            xmlReader.Read();
            xmlReader.Read();
            xmlReader.Read();
            if (xmlReader.Name.Equals("rows")) {
                rows = xmlReader.ReadElementContentAsInt();
            }
            if (xmlReader.Name.Equals("columns")) {
                columns = xmlReader.ReadElementContentAsInt();
            }
            if (xmlReader.Name.Equals("randomizeItemTypes")) {
                randomizeItemTypes = bool.Parse(xmlReader.ReadElementContentAsString());
            }
            for (int i = 0; i < MAX_ROWS; i++) {
                for (int j = 0; j < MAX_COLUMNS; j++) {
                    if (i < rows && j < columns) {
                        if (xmlReader.Name.Equals("slot")) {
                            bool enabled = bool.Parse(xmlReader.GetAttribute("enabled"));
                            if (enabled) {
                                xmlReader.Read();
                                if (xmlReader.Name.Equals("item")) {
                                    Tile.TypePrimary typePrimary = Tile.TypePrimary.UNKNOWN;
                                    Tile.TypeSecondary typeSecondary = Tile.TypeSecondary.NORMAL;
                                    xmlReader.Read();
                                    if (xmlReader.Name.Equals("typePrimary")) {
                                        typePrimary = (Tile.TypePrimary)Enum.Parse(typeof(Tile.TypePrimary), xmlReader.ReadElementContentAsString());
                                    }
                                    if (xmlReader.Name.Equals("typeSecondary")) {
                                        typeSecondary = (Tile.TypeSecondary)Enum.Parse(typeof(Tile.TypeSecondary), xmlReader.ReadElementContentAsString());
                                    }
                                    Tile tile = new Tile((int)typePrimary, (int)typeSecondary);
                                    matrixTiles[i, j] = tile;
                                    xmlReader.Read();
                                    xmlReader.Read();
                                } else {
                                    Tile tile = new Tile((int)Tile.TypePrimary.NONE, (int)Tile.TypeSecondary.NORMAL);
                                    matrixTiles[i, j] = tile;
                                }
                            } else {
                                Tile tile = new Tile(0, 0);
                                tile.Enabled = false;
                                matrixTiles[i, j] = tile;
                                xmlReader.Read();
                            }
                        }
                    } else {
                        Tile tile = new Tile(0, 0);
                        matrixTiles[i, j] = tile;
                    }
                }
            }
        }
    }

    public void DeleteLevel() {
        FileInfo fileInfo;
        string fullName;

        fullName = Application.dataPath + "/Resources/Levels/" + dropDownOptions[level] + ".xml";
        fileInfo = new FileInfo(fullName);
        fileInfo.Delete();

        fullName = Application.dataPath + "/Resources/Levels/" + dropDownOptions[level] + ".xml.meta";
        fileInfo = new FileInfo(fullName);
        fileInfo.Delete();

        ResetLevelCreation();

        dropDownOptions.RemoveAt(level);
        if (dropDownOptions.Count != 0) {
            LoadLevel(0);
            newLevel = 0;
        } else {
            CreateLevelPopup window = new CreateLevelPopup();
            window.ShowUtility();
            window.Focus();
        }
        changeCurrentLevel = true;

        AssetDatabase.Refresh();
    }
}
