﻿using UnityEngine;
using System;

public class Tile {
    public enum SlotCover { //ice; when we hit it becomes weaker and weaker until destroyed
        COVER_0 = 0,
        COVER_1 = 1,
        COVER_2 = 2,
        COVER_3 = 3
    }

    public enum SlotMovement {
        NO_MOVEMENT = 0,
        MOVE_LEFT = 1,
        MOVE_RIGHT = 2,
        MOVE_UP = 3,
        MOVE_DOWN = 4
    }

    public enum TypePrimary {
        UNKNOWN = 0, TYPE_1 = 1, TYPE_2 = 2, TYPE_3 = 3, TYPE_4 = 4, TYPE_5 = 5, TYPE_6 = 6,
        BLOCKER = 7,
        COLOR_BOMB = 8,
        NONE = 9,
        LOCKER = 10,
        REGENERATE = 11, //jam that regenerates if no other jam has been hit in the last round
        REGENERATE_SOURCE = 12, //jam jar: cannot be destroyed; can produce jam on nearby positions
        NOT_MATCHABLE_BUT_MOVABLE = 13,
        GROW_ON_HIT_TYPE_1 = 14, GROW_ON_HIT_TYPE_2 = 15, GROW_ON_HIT_TYPE_3 = 16, GROW_ON_HIT_TYPE_4 = 17, GROW_ON_HIT_TYPE_5 = 18, GROW_ON_HIT_TYPE_6 = 19, // trees: when hit 3 times, they produce the respective fruit and then reset
        CLONE = 20,
        PRODUCE_ON_HIT = 21 //cow
    }
    public enum TypeSecondary {
        NORMAL = 0, HORIZONTAL_CLEANER = 1, VERTICAL_CLEANER = 2, SURROUNDING_CLEANER = 3, X_CLEANER = 4,
        BLOCKER_1 = 5, BLOCKER_2 = 6, BLOCKER_3 = 7,
        NOT_MATCHABLE_BUT_MOVABLE_1 = 8, NOT_MATCHABLE_BUT_MOVABLE_2 = 9, NOT_MATCHABLE_BUT_MOVABLE_3 = 10
    }

    public bool Enabled { get; set; }
    private string text1 = "", text2 = "";
    private TypePrimary typePrimaryTile;
    private TypeSecondary typeSecondaryTile;
    private Color colorTile;
    private int indexPrimary, indexSecondary;

    public Tile(int typePrimary, int typeSecondary) {
        Enabled = true;
        ChangePrimaryType(typePrimary);
        ChangeSecondaryType(typeSecondary);
    }

    public void ChangePrimaryType(int type) {
        typePrimaryTile = (TypePrimary)type;
        if (typePrimaryTile == TypePrimary.UNKNOWN) {
            colorTile = Color.white;
            text1 = "?";
        } else if (typePrimaryTile == TypePrimary.TYPE_1) {
            colorTile = Color.cyan;
            text1 = "1";
        } else if (typePrimaryTile == TypePrimary.TYPE_2) {
            text1 = "2";
            colorTile = Color.red;
        } else if (typePrimaryTile == TypePrimary.TYPE_3) {
            colorTile = Color.green;
            text1 = "3";
        } else if (typePrimaryTile == TypePrimary.TYPE_4) {
            colorTile = Color.blue;
            text1 = "4";
        } else if (typePrimaryTile == TypePrimary.TYPE_5) {
            colorTile = Color.yellow;
            text1 = "5";
        } else if (typePrimaryTile == TypePrimary.TYPE_6) {
            colorTile = Color.magenta;
            text1 = "6";
        } else if (typePrimaryTile == TypePrimary.BLOCKER) {
            colorTile = new Color(153, 51, 0);
            text1 = "BL";
        } else if (typePrimaryTile == TypePrimary.COLOR_BOMB) {
            colorTile = new Color(0, 102, 102);
            text1 = "CB";
        } else if (typePrimaryTile == TypePrimary.NONE) {
            colorTile = Color.white;
            text1 = "N/A";
        }
    }

    public void ChangeSecondaryType(int type) {
        typeSecondaryTile = (TypeSecondary)type;
        if (typeSecondaryTile == TypeSecondary.NORMAL) {
            text2 = "Normal";
        } else if (typeSecondaryTile == TypeSecondary.HORIZONTAL_CLEANER) {
            text2 = "Horzontal";
        } else if (typeSecondaryTile == TypeSecondary.VERTICAL_CLEANER) {
            text2 = "Vertical";
        } else if (typeSecondaryTile == TypeSecondary.SURROUNDING_CLEANER) {
            text2 = "Surrounding";
        }
    }

    public Color ColorTile {
        get {
            return colorTile;
        }
    }
    public TypePrimary TypePrimaryTile {
        get {
            return typePrimaryTile;
        }
    }
    public TypeSecondary TypeSecondaryTile {
        get {
            return typeSecondaryTile;
        }
    }

    public string ButtonText {
        get {
            return text1 + Environment.NewLine + text2;
        }
    }
}
