﻿using UnityEngine;
using UnityEditor;

public class CreateLevelPopup : EditorWindow {
    int levelNumber;
    void OnGUI() {
        levelNumber = EditorGUILayout.IntField("Name for the new level", levelNumber);
        var match3Editor = (EditorWindow.GetWindow(typeof(Match3Editor)) as Match3Editor);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Add level")) {
            match3Editor.AddLevelToDropdown(levelNumber);
            match3Editor.Focus();
            this.Close();
        }
        if (GUILayout.Button("Cancel")) {
            match3Editor.Focus();
            this.Close();
        }
        GUILayout.EndHorizontal();
    }
}
