﻿using UnityEngine;
using UnityEditor;

public class DeleteLevelPopup : EditorWindow {
    void OnGUI() {
        GUILayout.Label("Are you sure you want to delete this level?");
        var match3Editor = (EditorWindow.GetWindow(typeof(Match3Editor)) as Match3Editor);
        if (GUILayout.Button("Yes")) {
            match3Editor.DeleteLevel();
            match3Editor.Focus();
            this.Close();
        }
        if (GUILayout.Button("Cancel")) {
            match3Editor.Focus();
            this.Close();
        }
    }
}
