# FunPlus Tile Blast! #

## Intro ##
Scope: Build a prototype. The priority was to pack as many features in as little time as possible.

The requirements for this Tile Blast game were built on top of one of my old college projects, which was a Match 3 game.
The implementation is on the **funplus** branch. For the original implementation, check out the **master** branch.

## How to use ##
There are 2 scenes. 
In order to access the level selection menu, start the game from **MainMenuScene**.
If you access the game directly from the **GameScene**, you will always play the first level.

Access the editor via **Window -> Match 3 Editor**. Select the level you want to edit from the dropdown. Click **Load Level**.
If you want to change an item type, then click **Add items**, then select the type, then click somewhere on the matrix to change the item type of that specific slot. **Unknown** represents a random item.
Notice that *TYPE_1, TYPE_2* etc have subtypes: *NORMAL, HORIZONTAL_CLEANER* etc. In the original implementation of the Match 3 engine, it supported different sprites for the power-ups, depending on the original color of the item. You can ignore these and leave to *NORMAL*.

## Features ##
- Level Editor
	- Note: If adding/removing levels, you must also update the **MenuManager**'s **levelTextAssets** field in the **MainMenuScene**
- Clicking a tile without any matching neighbours won't do anything
- Clicking a tile with at leas 1 matching neighbour will remove them all
- Feedbacks for the above features
- Boosters
	- If clicked a block of at least 6 matching tiles, then you get a bomb
	- If clicked a block of 5 tiles on the same axis, you will get a dynamite 
	- If clicked a block of 4 tiles on the same axis, you will get a power-up (horizontal or vertical)
- Given any number of rows or columns, the game will fit in the screen
- A very basic scoring system
- The stone block, which will not let items fall, if they live above it. See this one in action in level 1
- Diagonal item falling. If a slot is blocked by a stone block which lives on top of it, it can still receive items from the left or right of the stone block (if these slots don't have a stone block themselves). See this one in action in level 1
- Shuffle. If there are no more possible matches, then re-shuffle. See this one in action in level 50.
- A very basic level selection

## Restrictions ##
This is a prototype. If the complexity of the game increases, it is recommended to refactor this as described below in the **To Do** section.
Do not mess around too much with the namings of the xml level file, it's untested what happens. If you want to test new scenarios, just modify and save an already existing level.

## To Do ##
- While keeping already implemented gameplay logic, add code separation in **GameManager**. Split the work into multiple classes implementing the same interface, acting like a sequence of steps.
- In the above mentioned steps, split the code that modifies data (the matrix) from the work that handles animations (eg. falling).
- Save/load levels async
- Replace the obsolete XML data storage with JSON


